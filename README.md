* This project has been created to demonstrate implementation and usage of @angular-redux/store library. 
* The project is a simple shopping cart. On load, the home component pulls products list from service. The fetched data is pushed to store. 
* The Home component subscribes to products list from redux store and pushes the same to child component (products-list component) where data is captured with @Input variable. 
* When search component dispatches search parameters on search action, the redux store filters products list based on search parameters and hence the products list gets updated. 
* Products list component loops through products array and loads data in loop to product component. 
* Product component has individual row of data with option to Add to cart / remove . 
* Add to cart / remove actions are dispatched on click , which updates redux store accordingly and the changes get reflected in my cart section in header component which has 
 subscribed to cart items data from store. 

# AngularRedux

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.15.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
