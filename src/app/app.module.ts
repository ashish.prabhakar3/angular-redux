import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgRedux, NgReduxModule } from '@angular-redux/store';

import { ProductlistComponent } from './productlist/productlist.component';
import {ShopReducer, InitialState, initialState} from './reducer';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ProductComponent } from './product/product.component';
import { HeaderComponent } from './header/header.component';
import { DataService } from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,    
    ProductlistComponent,
    HomeComponent,
    SearchComponent,
    ProductComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule,
    AppRoutingModule, NgReduxModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(ngRedux: NgRedux<InitialState>) {
    ngRedux.configureStore(ShopReducer, initialState);
  }

}
