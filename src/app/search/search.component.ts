import { Component, OnInit } from '@angular/core';
import { DataService } from './../services/data.service';
import { Category } from './../models/category';
import { Groups } from '../models/groups';
import { Observable } from 'rxjs';
import { NgRedux } from '@angular-redux/store';
import { IsearchFilters } from '../searchfilters';
import { InitialState } from '../reducer';
import { FilterItems } from '../actions/actions';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  categoryList$: Observable<Category[]>;
  groupsList$: Observable<Groups[]>;
  
  model: IsearchFilters = {
    catName: "",
    groupName: ""
  };
  
  constructor(private dataService: DataService,  private ngRedux: NgRedux<InitialState>) { }

  ngOnInit() {
    this.dataService.getAllCategories().subscribe(res => {
      this.categoryList$ = res['body'];      
    });

    this.dataService.getAllGroups().subscribe(res => {
      this.groupsList$ = res['body'];
    });

  }

  onSubmit(){    
    this.ngRedux.dispatch(FilterItems(this.model));
    
  }

}
