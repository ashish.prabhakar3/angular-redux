import { ActionTypes } from './actions/actions';
import { Products } from './models/products';
import { Category } from './models/category';
import { Groups } from './models/groups';
import { IsearchFilters } from './searchfilters';
import { filter } from 'minimatch';

export interface InitialState {
    items: Array<Products>;
    cart: Array<Products>;
    filters: IsearchFilters;
    filterItems:Array<Products>;
}

export const initialState = {
    items: [],
    cart: [],
    filters:{catName:"", groupName: ""},
    filterItems:[]
}

export function ShopReducer(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.LoadSuccess:
            return {
                ...state,
                items: [...action.payload],
                filterItems: [...action.payload]
            };
        case ActionTypes.Add:
            return {
                ...state,
                cart: [...state.cart, action.payload]
            };
        case ActionTypes.Remove:
            return {
                ...state, 
                cart: [...state.cart.filter(item => item !== action.payload)]
            };
        case ActionTypes.Filter:
            let filterQuery = {};
            if(action.payload.catName != "") filterQuery['catName'] = action.payload.catName;
            if(action.payload.groupName != "") filterQuery['groupName'] = action.payload.groupName;
            return {
                ...state,
                filters:action.payload,
                filterItems: [...state.items.filter(item => {
                    for(let key in filterQuery){
                        if(item[key] === undefined || item[key] != filterQuery[key])
                            return false;                        
                    }
                    return true;
                })]
            };                                               
        default: 
        return state;
    }
}