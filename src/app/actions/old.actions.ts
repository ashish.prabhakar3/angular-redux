export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO ='TOGGLE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const REMOVE_ALL_TODOS = 'REMOVE_ALL_TODOS';
export const SEARCH_FILTER = 'SEARCH_FILTER'; 
export const PRODUCTSLIST_FETCH = 'PRODUCTSLIST_FETCH';
export const FETCHED_PRODUCTS = 'FETCHED_PRODUCTS';