import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Products } from '../models/products';
import { Observable } from 'rxjs';
import { NgRedux } from '@angular-redux/store';
import { ShopReducer, InitialState } from './../reducer';
import { LoadItems } from './../actions/actions';
import { Category } from '../models/category';
import { Groups } from '../models/groups';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiPath = environment.apiPath;

  constructor(private http: HttpClient, private ngRedux: NgRedux<InitialState>) { }

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.apiPath + 'category');
  }
  
  getAllGroups(): Observable<Groups[]>{
    return this.http.get<Groups[]>(this.apiPath + 'group');
  }

  getProducts(catName:string = "", groupName:string = ""){    
     this.http.get(this.apiPath + 'product')
      .subscribe((products: Array<Products>) => {
        this.ngRedux.dispatch(LoadItems(products['body']));
      });
  }

}
