import { ITodo } from './todo';
import { IsearchFilters } from './searchfilters';
import { ADD_TODO, TOGGLE_TODO, REMOVE_TODO, REMOVE_ALL_TODOS, SEARCH_FILTER, FETCHED_PRODUCTS } from './actions/old.actions';
import { Action } from 'rxjs/internal/scheduler/Action';
import { DataService } from './services/data.service';
import { Products } from './models/products';

export interface IAppState {
    searchFilters: IsearchFilters[];
    products:Products[];
    todos: ITodo[];
    lastUpdate: Date;
}

export const INITIAL_STATE: IAppState = {
    searchFilters: [{"catName":"men","groupName":""}],
    products:[],    
    todos: [],
    lastUpdate: null
} 

export function rootReducer(state: IAppState, action:any): IAppState {
    let productsListData$: Products[];
    console.log("reached rootreducer", action);
    switch (action.type) {
        case FETCHED_PRODUCTS:
            return Object.assign({}, state, {
                searchFilters: {catName:state.searchFilters['catName'], groupName: state.searchFilters['groupName']},
                products: action.products,
                lastUpdate: new Date()
            })
        case SEARCH_FILTER:
            return Object.assign({}, state, {
                searchFilters: {catName:action.searchFilters.catName, groupName: action.searchFilters.groupName},
                products: productsListData$,
                lastUpdate: new Date()
            })            
        case ADD_TODO: 
            action.todo.id = state.todos.length + 1;
            return Object.assign({}, state, {
                todos: state.todos.concat(Object.assign({}, action.todo)),
                lastUpdate: new Date()
            })
        case TOGGLE_TODO: 
            var todo = state.todos.find(t => t.id === action.id);
            var index = state.todos.indexOf(todo);
            return Object.assign({}, state, {
                todos: [
                    ...state.todos.slice(0, index),
                    Object.assign({}, todo, {isCompleted: !todo.isCompleted}),
                    ...state.todos.slice(index + 1)
                ],
                lastUpdate: new Date()
            })
        case REMOVE_TODO: 
            return Object.assign({}, state, {
                todos: state.todos.filter(t => t.id !== action.id),
                lastUpdate: new Date()
            })
            case REMOVE_ALL_TODOS: 
                return Object.assign({}, state, {
                    todos: [],
                    lastUpdate: new Date()
                })
    }
    return state;
}