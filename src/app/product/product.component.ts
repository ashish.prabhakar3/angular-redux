import { Component, OnInit, Input } from '@angular/core';
import { AddToCart, RemoveFromCart } from '../actions/actions';
import { NgRedux } from '@angular-redux/store';
import { InitialState } from '../reducer';
import { Products } from '../models/products';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  constructor(private ngRedux: NgRedux<InitialState>) { }
  inCart = false;
  @Input() product: Products;

  ngOnInit() {
  }

  addToCart(item: Products) {
    this.ngRedux.dispatch(AddToCart(item));
    this.inCart = true;
  }

  removeFromCart(item: Products) {
    this.ngRedux.dispatch(RemoveFromCart(item));
    this.inCart = false;
  }

}
