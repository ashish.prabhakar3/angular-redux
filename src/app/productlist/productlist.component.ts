import { Component, OnInit, Input } from '@angular/core';
import { Products } from './../models/products';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {
  @Input() prods: Array<Products>;
  constructor() { }
  
  ngOnInit() {    
  }

}
