import { Component, OnInit, Input } from '@angular/core';
import { Products } from '../models/products';
import { NgRedux } from '@angular-redux/store';
import { InitialState } from '../reducer';
import { IsearchFilters } from '../searchfilters';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cart: Array<Products>;
  searchFilters: IsearchFilters;

  constructor( private ngRedux: NgRedux<InitialState>) {
    this.ngRedux.select<Array<Products>>('cart')
    .subscribe((items: Array<Products>) => {
      this.cart = items;
    });
  }

  ngOnInit() {
  }

}
