import { Component, OnInit } from '@angular/core';
import { GetItems } from '../actions/actions';
import { Products } from './../models/products';
import { NgRedux, select } from '@angular-redux/store';
import { InitialState } from '../reducer';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private ngRedux: NgRedux<InitialState>, private dataService: DataService) { }

  @select('filterItems') items$: Observable<Array<Products>>;

  ngOnInit() {
    this.dataService.getProducts();
  }

}
